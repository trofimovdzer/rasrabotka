#include <stdio.h>
#include <string.h>
int main()
{
	char s[256];
	char* p;
	char* parr[256];
	int nw, inWord;
	int i;

	printf("Enter a string, please.\n");
	fgets(s, 256, stdin);
	s[strlen(s) - 1] = '\0';
	nw = 0;
	inWord = 0;
	p = s;
	while (*p)
	{
		if (*p != ' ' && inWord == 0)
		{	
			parr[nw] = p;
			nw++;
			inWord = 1;
		}
		else
			if (*p == ' ' && inWord == 1)
				inWord = 0;
		p++;
	}
	printf("Number words are %d\n", nw);
	for(i = 0; i < nw; i++)
	{
		p = parr[i];
		while(*p != ' ' && *p)
			putchar(*p++);
		printf("\t%s-\t%d symbols\n", p - parr[i] > 8?"":"\t", p - parr[i]);
	}

	return 0;
}
