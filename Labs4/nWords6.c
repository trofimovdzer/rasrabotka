#include <stdio.h>
#include <string.h>
int main()
{
	char s[1024];
	char *p1, *p2, *pm;
	int max;

	printf("Enter a string, please.\n");
	fgets(s, 1024, stdin);
	s[strlen(s)-1] = '\0';
	if (strlen(s) == 0)
	{
		printf("Error! Empty string.\n");
		exit(1);
	}
	p1 = s;
	max = 0;
	while(*p1)
	{
		p2 = p1 + 1;
		while(*p2 && *p2 == *p1)
			p2++;
		if (p2 - p1 > max)
		{
			max = p2 - p1;
			pm = p1;
			p1 = p2 -1;
		}
		p1++;
	}
	printf("Tle longest sequence of one symbol:\n");
	p1 = pm;
	while (p1 < pm + max)
		putchar(*p1++);
	putchar('\n');
	printf("%d symbols\n", max);

	return 0;
}


