#ifndef _HFILE_H_
#define _HFILE_H_
#define LEN 256
#define NEX 5

typedef enum boolian {false, true} bool;
typedef unsigned char UC;
typedef struct byte {
	int ch;
	long friq;
	char str[LEN];
	struct byte * left;
	struct byte * right;
} Byte;
struct st_char {
	UC b0 : 1;
	UC b1 : 1;
	UC b2 : 1;
	UC b3 : 1;
	UC b4 : 1;
	UC b5 : 1;
	UC b6 : 1;
	UC b7 : 1;
};
typedef union bit_char {
	UC dig;
	struct st_char bits;
} Bit_char;

void extract(FILE * fp, char * name);
void zip(FILE *fp, char * name);
Byte * builTree(Byte ** parr, int count);
void free_tree(Byte * root);

#endif