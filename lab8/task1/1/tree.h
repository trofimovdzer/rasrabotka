#ifndef _TREE_H_H
#define _TREE_H_H
#define LEN 20
#define LENSTR 80
#define NWORD 100
struct node {
	char name[LEN];
	int count;
	struct node * right;
	struct node * left;
};
typedef struct node Node;
Node * addNode(Node * root, char * name);
void printTree(Node * root);
void SearchAndAdd(Node * root, char * name);
void cleanStr(char * str);
void wordSplitting(char * str, char * parr[NWORD], int * nw);
#endif