#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include <string.h>
#define H 10
#define W 29
#define UP 119
#define DOWN 115
#define LEFT 97
#define RIGHT 100
#define TRUE 1
#define FALSE 0
struct coord {
	int x;
	int y;
};
char nobuf()
{
	while(!kbhit());
	return getch();
}
int check(char c, struct coord *p, char arr[][W]);
int main()
{	
	struct coord point;
	char arr[H][W], choice = 1;
	int lose = TRUE;
	int i, j;
	FILE *fp;

	if((fp = fopen("stored.dat", "rb")) == NULL)
	{
		puts("Can't open file");
		exit(1);
	}
	fread(arr, sizeof(char) * W, H, fp);
	fclose(fp);
	for(i = 0; i < H; i++)
		for(j = 0; j < W; j++)
			if(arr[i][j] == 'x')
			{
				point.x = i;
				point.y = j;
				break;
			}
	while(choice != 'q' && lose)
	{
		system("cls");
		puts("LABIRINT");
		puts("Press \"a\" to left, \"d\" to right, \"s\" to down, \"w\" to up.");
		puts("\"q\" to qxit.");
		for(i = 0; i < H; i++)
			puts(arr[i]);
		choice = nobuf();
		switch(check(choice, &point, arr))
		{
		case -1:
			putchar('\b');
			break;
		case 0:
			break;
		case 1:
			system("cls");
			puts("Bingo");
			puts("Press any kye");
			nobuf();
			lose = FALSE;
			break;
		}
	}

	return 0;
}
int check(char c, struct coord *p, char arr[][W])
{
	if(strchr("asdwq", c) == NULL)
		return -1;
	switch(c)
	{
	case 'q':
		return 0;
	case UP:
		if((p->x - 1)>= 0 && arr[p->x - 1][p->y] != '#')
		{
			arr[p->x][p->y] = ' ';
			p->x = p->x - 1;
			arr[p->x][p->y] = 'x';
			return 0;
		}
		else if((p->x - 1) < 0)
			return 1;
		else return -1;
	case DOWN:
		if((p->x + 1)< H && arr[p->x + 1][p->y] != '#')
		{
			arr[p->x][p->y] = ' ';
			p->x = p->x + 1;
			arr[p->x][p->y] = 'x';
			return 0;
		}
		else if((p->x + 1) >= H)
			return 1;
		else return -1;
	case LEFT:
		if((p->y - 1) >= 0 && arr[p->x ][p->y - 1] != '#')
		{
			arr[p->x][p->y] = ' ';
			p->y = p->y - 1;
			arr[p->x][p->y] = 'x';
			return 0;
		}
		else if((p->y - 1) < 0)
			return 1;
		else return -1;
	case RIGHT:
		if((p->y + 1)< W && arr[p->x][p->y + 1] != '#')
		{
			arr[p->x][p->y] = ' ';
			p->y = p->y + 1;
			arr[p->x][p->y] = 'x';
			return 0;
		}
		else if((p->y + 1) >= W)
			return 1;
		else return -1;
	}

}




