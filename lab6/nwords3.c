#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1024
void printWord(char *parr[N], int nw)
{
	int i, count = nw;
	char *p;
	if(nw == 0)
	{
		putchar('\n');
		return 0;
	}
	while(1)
	{
		i = rand() % nw;
		if(parr[i] != NULL)
		{
			p = parr[i];
			while(*p && *p != ' ')
			{
				putchar(*p);
				p++;
			}
			count--;
			if(count)
				putchar(' ');
			else
			{
				putchar('\n');
				break;
			}
			parr[i] = NULL;
		}
	}
}
int getWord(char *arr, char *parr[N])
{
	int i, nw = 0, fl = 0;
	char *p;
	

	p = arr;
	while(*p)
	{
		if(*p != ' ' && fl == 0)
		{
			fl = 1;
			parr[nw++] = p;
		}
		else if(*p == ' ' && fl == 1)
			fl = 0;
		p++;
	}

	return nw;
}
int main(int argc, char *argv[])
{
	char arr[N];
	char *parr[N];
	int nw;
	FILE *in;
	
	if (argc < 2)
	{
		printf("Error. Enter the name for source file .\n");
		exit(1);
	}
	if((in = fopen(argv[1], "r")) == NULL)
	{
		printf("Error. Can't open file.\n");
		exit(2);
	}
	while(fgets(arr, N, in))
	{
		arr[strlen(arr) - 1] = '\0';
		nw = getWord(arr, parr);
		srand(time(NULL));
		printWord(parr, nw);
	}

	return 0;
}