#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1024
void printWord(char *parr[N], int nw)
{
	int i, count = nw;
	char *p;
	while(1)
	{
		i = rand() % nw;
		if(parr[i] != NULL)
		{
			p = parr[i];
			while(*p && *p != ' ')
			{
				putchar(*p);
				p++;
			}
			count--;
			if(count)
				putchar(' ');
			else
			{
				putchar('\n');
				break;
			}
			parr[i] = NULL;
		}
	}
}
int getWord(char *arr, char *parr[N])
{
	int i, nw = 0, fl = 0;
	char *p;
	

	p = arr;
	while(*p)
	{
		if(*p != ' ' && fl == 0)
		{
			fl = 1;
			parr[nw++] = p;
		}
		else if(*p == ' ' && fl == 1)
			fl = 0;
		p++;
	}

	return nw;
}
int main()
{
	char arr[N];
	char *parr[N];
	int nw;

	printf("Enter a string, please.\n");
	fgets(arr, N, stdin);
	arr[strlen(arr) - 1] = '\0';
	nw = getWord(arr, parr);
	srand(time(NULL));
	printWord(parr, nw);

	return 0;
}

			



