#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1024
int getWord(char *arr, char *parr[N])
{
	int i, nw = 0, fl = 0;
	char *p;
	

	p = arr;
	while(*p)
	{
		if(*p != ' ' && fl == 0)
		{
			fl = 1;
			parr[nw++] = p;
		}
		else if(*p == ' ' && fl == 1)
		{
			fl = 0;
			*p = '\0';
		}
		p++;
	}

	return nw;
}
void printWords(char *p)
{
	int i, temp, len = strlen(p);
	
	if(len < 4)
		printf("%s ", p);
	else
	{
		putchar(*p);
		p++;
		i = len = len - 2;
		while(i)
		{
			temp = rand()%len;
			if(*(p + temp) != '\0')
			{
				putchar(*(p + temp));
				*(p + temp) = '\0';
				i--;
			}
		}
		putchar(*(p+len));
		putchar(' ');
	}
}
			


int main(int argc, char *argv[])
{
	char arr[N];
	char *parr[N];
	FILE *in, *out;
	int ch, n, nw, i;

	if(argc < 2)
	{
		printf("Error! Not file.\n");
		exit(1);
	}
	in = fopen(argv[1], "r");
	if(in == NULL)
	{
		printf("Error. I can't open file for read.\n");
		exit(2);
	}
	out = fopen("result.txt", "w");
	if(out == NULL)
	{
		printf("Error. I can't open file for write.\n");
		exit(3);
	}
	n = 0;
	srand(time(NULL));
	while((ch = fgetc(in)) != EOF)
		if(ch == '\n')
		{
			arr[n] = '\0';
			n = 0;
			printf("%s\n", arr);
			nw = getWord(arr, parr);
			//---------------����� �����-----------------
			for(i = 0; i < nw; i++)
				printWords(parr[i]);
			putchar('\n');
			//-------------------------------------------
		}
		else
			arr[n++] = (char)ch;
	fclose(in);
	fclose(out);

	return 0;
}



