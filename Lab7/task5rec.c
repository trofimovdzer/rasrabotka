//����� �������� � ������� ��������
#include <stdio.h>
#include <time.h>
#define N 45
typedef unsigned long long ULL;
ULL func(int n)
{
	if(n == 1 || n == 2)
		return 1;
	else
		return func(n - 1) + func(n - 2);
}
int main()
{
	int i;
	clock_t begin, end;
	float result;

	for(i = 1; i <= N; i++)
	{
		begin = clock();
		func(i);
		end = clock();
		result = (float)(end - begin)/CLOCKS_PER_SEC;
		printf("%d\t\t%f\n", i, result);
	}

	return 0;
}