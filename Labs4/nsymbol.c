#include <stdio.h>
#include <string.h>
int main()
{
	char str[1024];
	int nsym[256] = {0};
	char *p;
	int max;
	int c, i;
	

	printf("Enter a string, please.\n");
	fgets(str, 1024, stdin);
	str[strlen(str)-1] = '\0';
	p = str;
	while (*p)
	{
		nsym[*p]++;
		p++;
	}
	for(;;)
	{
		max = 0;
		for(i = 0; i < 256; i++)
			if (nsym[i] > max)
			{
				c = i;
				max = nsym[i];
			}
		if (max == 0)
			break;
		else
		{
			printf("|\t%c| \t%d|\n",  c, max);
			nsym[c] = 0;
		}
	}

	return 0;
}

		