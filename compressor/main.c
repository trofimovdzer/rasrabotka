#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

extern const char format[3]; 

int main(int argc, char *argv[]) //argv[1] - filename, argv[2] - ZIP or EXTRACT
{
	FILE* fp;
	
	//check command line arguments
	assert(argc == 3);
	assert((fp = fopen(argv[1], "rb")) != NULL);
	if(strcmp(argv[2], "-z") == NULL)
		zip(fp, argv[1]);
	else if(strcmp(argv[2], "-e") == NULL)
		extract(fp, argv[1]);
	else
	{
		puts("Uncorrect second comand line argument");
		abort();
	}

	return 0;
}

