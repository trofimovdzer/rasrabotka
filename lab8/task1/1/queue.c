#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

static void CopyToNode(Item item, Node * pn);
static void CopyToItem(Node *pn, Item *pi);

void InitializeQueue(Queue * pq)
{
	pq->front = pq->rear = NULL;
	pq->items = 0;
}
bool QueueIsFull(Queue * pq)
{
	if(pq->items == MAXQUEUE)
		return true;
	else 
		return false;
}
bool QueueIsEmpty(Queue * pq)
{
	if(pq->items == 0)
		return true;
	else
		return false;
}
int QueueItemCount(const Queue * pq)
{
	return pq->items;
}
bool EnQueue(Item item, Queue * pq)
{
	Node * pnew;

	if(QueueIsFull(pq))
		return false;
	pnew = (Node *)malloc(sizeof(Node));
	if(pnew == NULL)
	{
		puts("Not enough memory.");
		exit(1);
	}
	CopyToNode(item, pnew);
	pnew->next = NULL;
	if(QueueIsEmpty(pq))
		pq->front = pnew;
	else
		pq->rear->next = pnew;
	pq->rear = pnew;
	pq->items++;

	return 0;
}
bool DeQueue(Item * pitem, Queue * pq)
{
	Node * pd;

	if(QueueIsEmpty(pq))
		return false;
	CopyToItem(pq->front, pitem);
	pd = pq->front;
	pq->front = pq->front->next;
	free(pd);
	pq->items--;
	if(pq->items == NULL)
		pq->rear = NULL;

	return true;
}
void EmptyTheQueue(Queue * pq)
{
	Item dummy;

	while(!QueueIsEmpty(pq))
		DeQueue(&dummy, pq);
}
static void CopyToNode(Item item, Node * pn)
{
	pn->item = item;
}
static void CopyToItem(Node * pn, Item * pi)
{
	*pi = pn->item;
}

		