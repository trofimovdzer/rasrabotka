//������� ����. ��������� �����������. ������������� ������� �� ������� �������������.
#include <stdio.h>
#include <stdlib.h>

struct SYM {
	int symbol;
	int number_of_symbols;
	double frequency;
};
void sort(struct SYM arr[])
{
	int i, j;
	struct SYM temp;

	for(i = 0; i < 256; i++)
		for(j = i + 1; j < 256; j++)
			if(arr[i].frequency < arr[j].frequency)
			{
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
}
				
int main(int argc, char * argv[])
{
	struct SYM arr[256] = {0};
	unsigned long long number_of_all_symbols = 0;
	FILE * fp;
	int ch, i;

	if(argc != 2)
	{
		puts("Uncorrect comand line arguments.");
		exit(1);
	}
	if((fp = fopen(argv[1], "rt")) == NULL)
	{
		printf("Can't open the file %s.\n", argv[1]);
		exit(2);
	}
	while((ch = getc(fp)) != EOF)
	{
		number_of_all_symbols++;
		arr[ch].number_of_symbols++;
	}
	fclose(fp);
	for(i = 0; i < 256; i++)
	{
		arr[i].symbol = i;
		if(arr[i].number_of_symbols)
			arr[i].frequency = (double)arr[i].number_of_symbols / number_of_all_symbols;
	}
	sort(arr);
	for(i = 0; i < 256; i++)
		if(arr[i].frequency > 0)
			switch(arr[i].symbol)
			{
			case '\t':
				printf("TAB [%g]\n",  arr[i].frequency);
				break;
			case ' ':
				printf("SPACE [%g]\n", arr[i].frequency);
				break;
			case '\n':
				printf("\\n [%g]\n", arr[i].frequency);
				break;
			default:
				printf("%c [%g]\n", arr[i].symbol, arr[i].frequency);
			}
	
	return 0;
}


	


