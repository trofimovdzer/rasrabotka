#include <stdio.h>
#include <string.h>
int main()
{
	char s[256];
	char* p;
	char* parr[256];
	int nw, inWord, n;
	int i;

	printf("Enter a string, please.\n");
	fgets(s, 256, stdin);
	printf("Enter the number of words, which must be displayed on the screen.\n");
	scanf("%d", &n);
	s[strlen(s) - 1] = '\0';
	nw = 0;
	inWord = 0;
	p = s;
	while (*p)
	{
		if (*p != ' ' && inWord == 0)
		{	
			parr[nw] = p;
			nw++;
			inWord = 1;
		}
		else
			if (*p == ' ' && inWord == 1)
				inWord = 0;
		p++;
	}
	if (n > nw)
	{
		printf("Error! Data is incorrect.\n");
		exit(1);
	}
	p = parr[n-1];
	while (*p != ' ' && *p != '\0')
		putchar(*p++);
	putchar('\n');

	return 0;
}
