#include <stdio.h>
#include <string.h>
#include "hfile.h"
#include <assert.h>


const char format[3];

void extract(FILE * fp, char * name)
{
	char our_format[4];
	UC count, temp, tail;
	int i, ch;
	Byte * arr, ** parr, * root, * pnode;
	char s[8], old_extension[NEX], * old_name;
	FILE * ftemp, *fsource;
	Bit_char sym;

	fread(our_format, 1, 3, fp);
	our_format[3] = '\0';
	assert(strcmp(our_format, format) == 0);//check
	fread(&count, 1, 1, fp);
	arr = (Byte *)malloc((count + 1) * sizeof(Byte));
	parr = (Byte **)malloc((count + 1) * sizeof(Byte *));
	for(i = 0; i <= count; i++)
	{
		fread(&temp, 1, 1, fp);
		arr[i].ch = (int)temp;
		arr[i].right = arr[i].left = NULL;
		fread(&arr[i].friq, sizeof(long), 1, fp);
		parr[i] = arr + i;
	}
	//buiding tree
	root = builTree(parr, count);
	free(parr);
	//reading tail
	fread(&tail, 1, 1, fp);
	//reading old extension
	fread(&old_extension, 1, NEX, fp);
	//saving to temp file
	ftemp = fopen("temp2.txt", "wb");
	while((ch = getc(fp)) != EOF)
	{
		sym.dig = (UC)ch;
		s[0] = sym.bits.b0 + '0';
		s[1] = sym.bits.b1 + '0';
		s[2] = sym.bits.b2 + '0';
		s[3] = sym.bits.b3 + '0';
		s[4] = sym.bits.b4 + '0';
		s[5] = sym.bits.b5 + '0';
		s[6] = sym.bits.b6 + '0';
		s[7] = sym.bits.b7 + '0';
		fwrite(s, 1, 8, ftemp);
	}
	fclose(fp);
	//writing token in the end of source code 
	fseek(ftemp, -(long)tail, SEEK_END);
	putc('#', ftemp);
	fclose(ftemp);
	//obtaining old name of file
	old_name = (char *)malloc(LEN);
	strcpy(old_name, name);
	old_name[strlen(old_name) - 4] = '\0';
	strcat(old_name, old_extension);
	//builing source code with help tree
	fsource = fopen(old_name, "wb");
	ftemp = fopen("temp2.txt", "rb");
	pnode = root;
	while((ch = getc(ftemp)) != '#')
	{
		if(ch == '0')
			pnode = pnode->left;
		else
			pnode = pnode->right;
		if(pnode->left == NULL && pnode->right == NULL)
		{
			putc(pnode->ch, fsource);
			pnode = root;
		}
	}
	//close all files and free memory
	fclose(fsource);
	fclose(ftemp);
	unlink("temp2.txt");
	free(arr);
}