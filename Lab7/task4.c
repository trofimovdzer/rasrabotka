/*�������� ��������� ������� ��������� ������ ������������ � ����������� ����������*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int fnum(int m)
{
	if(m == 0)
		return 1;
	else
		return 2 * fnum(m - 1);
}
int sum(int *arr, int n)
{
	int i, tot = 0;

	for(i = 0; i < n; i++)
		tot +=arr[i];

	return tot;
}
int sumrec(int *arr, int n)
{
	if(n == 1)
		return arr[0];
	else
		return (sumrec(arr, n / 2) + sumrec(arr + n / 2, n - n / 2));
}
int main(int argc, char *argv[])
{
	int m, number, i, s, sr;
	int *arr;
	clock_t begin, end;
	double t;

	if(argc != 2)
	{
		perror("Not correct comand line argumets\n");
		exit(1);
	}
	m = atoi(argv[1]);
	number = fnum(m);
	arr = (int*)(malloc(sizeof(int) * number));
	srand(time(NULL));
	for(i = 0; i < number; i++)
		arr[i] = rand();
	//-------------------------------------------------
	puts("Go!Go!Go!");
	begin = clock();
	s = sum(arr, number);
	end = clock();
	t = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Summa(standart) is %d. Time is %f.\n", s, t);
	//-------------------------------------------------
	begin = clock();
	sr = sumrec(arr, number);
	end = clock();
	t = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("Summa(rekurs) is %d. Time is %f.\n", sr, t);
	//-------------------------------------------------
	free(arr);

	return 0;
}






