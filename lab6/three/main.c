#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#define M 80
#define N 40
#define FROM 3
#define TO 6
void clear(char (*arr)[M])
{
	int i, j;

	for(i = 0; i < N; i++)
		for(j = 0; j < M; j++)
			arr[i][j] = ' ';
}
void triangle(int *hx, int *hy, char (*arr)[M], int h)
{
	int i;

	while(h)
	{
		arr[*hx][*hy] = '*';
		for(i = 1; i < h; i++)
		{
			arr[*hx][*hy + i] = '*';
			arr[*hx][*hy - i] = '*';
		}
		(*hx)--;
		h--;
	}
}
void print(char (*arr)[M])
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
			putchar(arr[i][j]);
		putchar('\n');
	}
}
void three(int *hx, int *hy, char (*arr)[M], int ht)
{
	while(ht)
	{
		triangle(hx, hy, arr, ht);
		ht--;
	}
}
int random(int from, int to)
{
	return from + rand() % (to - from  + 1);
}
int main()
{
	char arr[N][M];
	int hx, hy, h;
	while(1)
	{
		clear(arr);
		srand(time(NULL));
		//---------------------------Первичная инициалищация-----------
		hx = 39;
		h = random(FROM, TO);
		hy = h -1;
		//-------------------------------------------------------------
		while(hy + h - 1 < M)
		{
			hx = 39;
			three(&hx, &hy, arr, h);
			hy = hy + h;
			h = random(FROM, TO);
			hy = hy + h -1;
		}
		print(arr);
		Sleep(2000);
		system("cls");
	}

	return 0;
}

