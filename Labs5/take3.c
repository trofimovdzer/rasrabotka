/*�������� ���������, ������� ����������� ������ � ����������, 
�� �������� �� ��� ������ �����������*/
#include <stdio.h>
#include <string.h>
#define N 256
int main()
{
	char str[N];
	int fl;
	char *p1, *p2;

	printf("Enter a string, please.\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';
	p1 = str;
	p2 = str + strlen(str) - 1;
	if(strlen(str) < 3)
	{
		printf("The string is palindrome.\n");
		exit(0);
	}
	fl = 1;
	while(p2 > p1)
	{
		if(*p2 != *p1)
		{
			fl = 0;
			break;
		}
		p1++;
		p2--;
	}
	if(fl == 1)
		printf("The string is palindrome.\n");
	else
		printf("The string is not palindrome.\n");

	return 0;
}
