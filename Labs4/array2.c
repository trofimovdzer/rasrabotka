#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int random(int l, int u);
int main()
{
	int arr[1024];
	int n;
	int i, j, z;
	int sum;
	int *max, *min, *upp, *low;

	printf("Enter size of array:\n");
	scanf("%d", &n);
	if (n < 3 || n >= 1024)
	{
		printf("Data is incorrect!\n");
		exit(1);
	}
	srand(time(NULL));
	for (i = 0; i < n; i++)
		if (random(0, 1))
			arr[i] = random(0, RAND_MAX);
		else
			arr[i] = - random(0, RAND_MAX);
	for (z = 0; z < n; z++)
		printf("%d ", arr[z]);
	printf("\nThis is our array.\n");
	max = min = arr;
	for(i = 1; i < n; i++)
	{
		if (*max < arr[i])
			max = arr + i;
		if (*min > arr[i])
			min = arr + i;
	}
	if (max == min || (max - min < 2 && min - max < 2))
	{
		printf("Error! Try uncreasing the size of the arrey.\n");
		exit(1);
	}
	printf("The maximum element is %d. It is %d.\n", max - arr, *max);
	printf("The minimum element is %d. It is %d.\n", min - arr, *min);
	if (max > min)
	{
		upp = max;
		low = min;
	}
	else
	{
		upp = min;
		low = max;
	}
	sum = 0;
	while(low < upp)
	{
		sum = sum + *low;
		low++;
	}	
	printf("The sum of numbers  beetwin %d element and %d element is %d.\n", max - arr, min - arr, sum);
		
	return 0;
}
int random(int l, int u)
{
	return l + rand() % (u - l + 1);
}
