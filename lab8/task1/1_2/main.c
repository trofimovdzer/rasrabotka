#include <stdio.h>
#include "tree.h"
extern Node * max_node;

int main(int argc, char * argv[])
{
	FILE * fp;
	char name[LEN], str[LENSTR];
	char * parr[NWORD];
	int i, count, nw;
	Node * root;

	if(argc < 3)
	{
		puts("Uncorrect comand line arguments");
		exit(1);
	}
	if((fp = fopen(argv[1], "rt")) == NULL)
	{
		printf("Can't open file %s\n", argv[1]);
		exit(2);
	}
	root = NULL;
	while(!feof(fp))
	{
		fscanf(fp, "%s", name);
		root = addNode(root, name);
	}
	
	fclose(fp);
	for(count = 2; count < argc; count++)
	{
		puts("--------------------------------------------");
		if((fp = fopen(argv[count], "rt")) == NULL)
		{
			printf("Can't open file %s\n", argv[2]);
			continue;
		}
		printf("Frequency of occurrence of word for the file: %s\n", argv[count]); 
		while(fgets(str, LENSTR, fp))
		{
			cleanStr(str);
			wordSplitting(str, parr, &nw);
			for(i = 0; i < nw; i++)
			{
				if(parr[i][0] < 'a' || parr[i][0] > 'z')
					continue;
				SearchAndAdd(root, parr[i]);
			}
		}
		fclose(fp);
		max_node = NULL;
		printTree(root);
		while(max_node)
		{
			printf("%s [%d]\n", max_node->name, max_node->count);
			max_node->count = 0;
			max_node = NULL;
			printTree(root);
		}
	}

	return 0;
}