/*������ ������. ������ ������� �� ����� � ������������ � ����.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define H 80
#define W 100
void chomp(char s[]);
int compare(const void *a, const void *b);
int main(int argc, char *argv[])
{
	char arr[W][H];
	char *parr[W];
	char *p;
	int i, j;
	int nstr;
	FILE *in, *out;
	char ch;

	if (argc < 2)
	{
		fprintf(stderr, "Error. No arguments.\n");
		exit(1);
	}
	if((in = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "Error. I coudn't open the file \"%s\".\n", argv[1]);
		exit(2);
	}
	i = 0;
	j = 0;
	parr[i] = arr[i];
	while((ch = getc(in)) != EOF)
	{
		arr[i][j] = ch;
		j++;
		if (ch == '\n')
		{	
			arr[i][j] = '\0';
			i++;
			parr[i] = arr[i];
			j = 0;
		}
	}
	arr[i][j] = '\0';
	if (fclose(in) != 0)
	{
		fprintf(stderr, "Error in closing file.\n");
		exit(3);
	}
	nstr = i;
	qsort(parr, nstr, sizeof(char *), compare);
	if ((out = fopen("result.txt", "w")) == NULL)
	{
		fprintf(stderr, "I coudn't open result.txt file.\n");
		exit(2);
	}
	for(i = 0; i <= nstr; i++)
	{	
		p = parr[i];
		while(*p)
			putc(*p++, out);
		putc('\n', out);
	}
	if (fclose(out) != 0)
		fprintf(stderr, "Error in closing file.\n");

	return 0;
}
void chomp(char s[])
{
	s[strlen(s)-1] = '\0';
}
int compare(const char *a, const char *b)
{
	return strlen(*(char **)a) - strlen(*(char **)b);
}