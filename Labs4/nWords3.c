#include <stdio.h>
#include <string.h>
int main()
{
	char s[256];
	char* p;
	char* parr[256];
	int nw, nmax, inWord;
	int i;
	char* max;

	printf("Enter a string, please.\n");
	fgets(s, 256, stdin);
	s[strlen(s) - 1] = '\0';
	nw = 0;
	inWord = 0;
	p = s;
	while (*p)
	{
		if (*p != ' ' && inWord == 0)
		{	
			parr[nw] = p;
			nw++;
			inWord = 1;
		}
		else
			if (*p == ' ' && inWord == 1)
				inWord = 0;
		p++;
	}
	nmax = 0;
	for(i = 0; i < nw; i++)
	{
		p = parr[i];
		while(*p != ' ' && *p)
			p++;
		if (p - parr[i] > nmax)
		{
			nmax = p - parr[i];
			max = parr[i];
		}
		*p = '\0';
	}
	if (nmax > 0)
		printf("The longest word is \"%s\". It consist of %d letters.\n", max, nmax);
	else
		printf("You entred empty string!\n");

	return 0;
}
