#include "tree.h"
#include "stdio.h"
#include <string.h>
#include <stdlib.h>

Node * max_node;

Node * addNode(Node * root,char * name)
{
	if(root==NULL)	{
		root=(Node *)malloc(sizeof(Node));
		strcpy(root->name, name);
		root->count=0;
		root->left=NULL;
		root->right=NULL;
	}
	else if(strcmp(root->name, name) > 0)
		root->left=addNode(root->left,name);
	else 
		root->right=addNode(root->right,name);
	
	return root;
}
void printTree(Node * root)
{
	if(root->left != NULL)
		printTree(root->left);
	if(max_node == NULL && root->count > 0)
		max_node = root;
	else if(max_node != NULL && max_node->count < root->count)
		max_node = root;
	if(root->right != NULL)
		printTree(root->right);
}
void SearchAndAdd(Node * root, char * name)
{
	if(root != NULL)
		if(strcmp(root->name, name) == 0)
			root->count++;
		else if(strcmp(root->name, name) > 0)
			SearchAndAdd(root->left, name);
		else
			SearchAndAdd(root->right, name);
}
void cleanStr(char * str)
{
	while(*str)
	{
		if((*str >= '#' && *str <= '/') || *str == '\n' || *str == '\t')
			*str = ' ';
		str++;
	}
}
void wordSplitting(char * str, char * parr[NWORD], int * nw)
{
	int inword = 0;

	*nw = 0;
	while(*str)
	{
		if(*str != ' ' && inword == 0)
		{
			parr[*nw] = str;
			(*nw)++;
			inword = 1;
		}
		else if(*str == ' ' && inword == 1)
		{
			*str = '\0';
			inword = 0;
		}
		str++;
	}
}


