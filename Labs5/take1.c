/*�������� ���������, ������� ��������� ������������ ������ ��������� �����
� ����������, � ����� ��������� �� � ������� ����������� ����� ������*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define H 80
#define W 100
void chomp(char s[]);
int compare(const void *a, const void *b);
int main()
{
	char arr[W][H];
	char *parr[W];
	int i;
	int nstr;

	printf("Enter text please.\n");
	i = -1;
	do
	{	i++;
		fgets(arr[i], H, stdin);
		chomp(arr[i]);
		parr[i] = arr[i];
	}
	while (strlen(arr[i]) > 0);
	nstr = i;
	if (nstr < 0)
	{
		printf("Error! You entred 0 strings.\n");
		exit(1);
	}
	qsort(parr, nstr, sizeof(char *), compare);
	printf("Result:\n");
	for(i = 0; i < nstr; i++)
		printf("%s\n", parr[i]);

	return 0;
}
void chomp(char s[])
{
	s[strlen(s)-1] = '\0';
}
int compare(const char *a, const char *b)
{
	return strlen(*(char **)a) - strlen(*(char **)b);
}
	