#include <stdio.h>
#include <string.h>
int main()
{
	char s[256];
	char* p;
	int nw, inWord;

	printf("Enter a string, please.\n");
	fgets(s, 256, stdin);
	s[strlen(s) - 1] = '\0';
	nw = 0;
	inWord = 0;
	p = s;
	while (*p)
	{
		if (*p != ' ' && inWord == 0)
		{
			nw++;
			inWord = 1;
		}
		else
			if (*p == ' ' && inWord == 1)
				inWord = 0;
		p++;
	}
	printf("Number words are %d\n", nw);

	return 0;
}
