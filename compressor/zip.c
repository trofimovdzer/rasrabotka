#include <stdio.h>
#include "hfile.h"
#include <assert.h>
#include <string.h>

const char format[3] = "ABC";
//local functions
static void complitArr(Byte * arr, FILE * fp);
static int mycomp(const void *p1, const void * p2);
static void sortParr(Byte ** parr, int count);
static void generate(Byte * root, char * s);
static UC FileToStr(FILE * fp, FILE * ftemp, Byte * root, Byte * arr);
static char * newName(char * name, char * newname, char * old_extension);
static FILE * createHead(FILE * fzip, const char * format, UC count, Byte arr[], UC tail, char * old_extension);
static void strToFile(FILE * fzip, FILE * ftemp);

void zip(FILE *fp, char * name)
{
	Byte * arr, ** parr;
	Byte * root;
	int i, count;//count poit to last no zero
	char * s, newname[LEN], old_extension[NEX]; 
	FILE * ftemp, *fzip;
	UC tail; 

	arr = (Byte *)malloc(LEN * sizeof(Byte));
	parr = (Byte **)malloc(LEN * sizeof(Byte *));
	//initialization of array 
	for(i = 0; i < LEN; i++)
	{
		arr[i].friq = 0;
		arr[i].ch = i;
		(arr[i].str)[0] = '\0';
		arr[i].left = arr[i].right = NULL;
	}
	//obtaining the number of each character
	complitArr(arr, fp);
	//sorting array
	qsort(arr, LEN, sizeof(Byte), mycomp);
	//creatint array of pointers for each element of array
	for(i = 0; i < LEN; i++)
	{
		if(arr[i].friq == 0)
			break;
		parr[i] = arr + i;
	}
	count = i - 1;
	//building tree
	root = builTree(parr, count);
	s = (char *)malloc(LEN);
	s[0] = '\0';
	generate(root, s);
	free(s);
	//obtaining lenght of tail
	ftemp = fopen("temp.txt", "wb");
	tail = FileToStr(fp, ftemp, root, arr);
	fclose(fp);
	fclose(ftemp);

	//---------------------------WRITE----------------------------
	ftemp = fopen("temp.txt", "rb");
	fzip = fopen(newName(name, newname, old_extension), "wb");
	fzip = createHead(fzip, format, (UC)count, arr, tail, old_extension);
	strToFile(fzip, ftemp);
	fclose(fzip);
	fclose(ftemp);
	unlink("temp.txt");
	puts("Copmres is finished");
}
void free_tree(Byte * root)
{
	if(root->left != NULL)
		free_tree(root->left);
	if(root->right != NULL)
		free_tree(root->right);
	free(root);
}
static void strToFile(FILE * fzip, FILE * ftemp)
{
	Bit_char sym;
	char s[8];

	while(fread(s, 1, 8, ftemp))
	{
		sym.bits.b0 = s[0];
		sym.bits.b1 = s[1];
		sym.bits.b2 = s[2];
		sym.bits.b3 = s[3];
		sym.bits.b4 = s[4];
		sym.bits.b5 = s[5];
		sym.bits.b6 = s[6];
		sym.bits.b7 = s[7];
		fwrite(&sym.dig, 1, 1, fzip);
	}
}
		

static FILE * createHead(FILE * fzip, const char * format, UC count, Byte arr[], UC tail, char * old_extension)
{
	int i;
	UC temp;
	long temp2;

	fwrite(format, 1, 3, fzip);
	fwrite(&count, 1, 1, fzip);
	for(i = 0; i <= count; i++)
	{
		temp = (UC)arr[i].ch;
		fwrite(&temp, 1, 1, fzip);
		temp2 = arr[i].friq;
		fwrite(&temp2, sizeof(long), 1, fzip);
	}
	fwrite(&tail, 1, 1, fzip);
	fwrite(old_extension, 1, NEX, fzip);

	return fzip;
}



	
static char * newName(char * name, char * newname, char * old_extension)
{
	int i, len;

	strcpy(newname, name);
	len = strlen(newname) - 1;
	old_extension[0] = '\0';
	for(i = len; i >= 0; i--)
		if(newname[i] == '.')
		{
			assert(i < len);
			strcpy(old_extension, newname + i);
			strcpy(newname + i, ".101\0");
			break;
		}
	
	return newname;
}
static UC FileToStr(FILE * fp, FILE * ftemp, Byte * root, Byte * arr)
{
	int ch, i;
	char * p;
	unsigned int tot = 0;

	rewind(fp);
	while((ch = getc(fp)) != EOF)
	{
		for(i = 0; i < LEN; i++)
			if(arr[i].ch == ch)
				break;
		p = arr[i].str;
		while(*p)
		{
			tot++;
			putc(*p, ftemp);
			p++;
		}
	}
	for(i = 0; i < (8 - (tot % 8)); i ++)
		putc('0', ftemp);

	return (UC)(8 - (tot % 8));
}

static void generate(Byte * root, char * s)
{
	char * news;

	if(root->right == NULL && root->left == NULL)
	{
		strcpy(root->str, s);
		return;
	}
	if(root->right != NULL)
	{
		news = (char *)malloc(LEN);
		news[0] = '\0';
		strcpy(news, s);
		strcat(news, "1\0");
		generate(root->right, news);
		free(news);
	}
	if(root->left != NULL)
	{
		news = (char *)malloc(LEN);
		news[0] = '\0';
		strcpy(news, s);
		strcat(news, "0\0");
		generate(root->left, news);
		free(news);
	}
}
static void complitArr(Byte arr[], FILE * fp)
{
	int ch;
	int i, tot = 0;

	while((ch = getc(fp)) != EOF)
	{
		arr[ch].friq++;
		tot++;
	}
	assert(tot > 2);
}
static int mycomp(const void *p1, const void * p2)
{
	const Byte * a = (const Byte *) p1;
	const Byte * b = (const Byte *) p2;

	if(a->friq < b->friq)
		return 1;
	if(a->friq == b->friq)
		return 0;
	return -1;
}
Byte * builTree(Byte ** parr, int count)
{
	Byte * p;

	p = (Byte *)malloc(sizeof(Byte));
	p->left = parr[count - 1];
	p->right = parr[count];
	p->friq = parr[count - 1]->friq + parr[count]->friq;
	(p->str)[0] = '\0';
	count--;
	parr[count] = p;
	sortParr(parr, count);
	if(count == 0)
		return p;

	return builTree(parr, count);
}
static void sortParr(Byte ** parr, int count)
{
	Byte * p;

	if(count == 0)
		return;
	if(parr[count]->friq > parr[count - 1]->friq)
	{
		p = parr[count];
		parr[count] = parr[count - 1];
		parr[count - 1] = p;
		count--;
		sortParr(parr, count);
	}
}

	


