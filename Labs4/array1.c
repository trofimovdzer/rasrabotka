#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int random(int low, int upp);
int main()
{
	int arr[1024];
	int n;
	int i, j, z;
	int sum;

	printf("Enter size of array:\n");
	scanf("%d", &n);
	if (n < 3)
	{
		printf("Data is incorrect!\n");
		exit(1);
	}
	srand(time(NULL));
	for (i = 0; i < n; i++)
		if (random(0, 1))
			arr[i] = random(0, RAND_MAX);
		else
			arr[i] = - random(0, RAND_MAX);	
	for (i = 0; i < n; i++)
		if (arr[i] < 0)
			break;
	for (j = n-1; j > 0; j--)
		if (arr[j] >= 0)
			break;
	if (i + 1 >= j)
	{
		printf("Size of array is too small.\n");
		exit(1);
	}
	for (z = 0; z < n; z++)
		printf("%d ", arr[z]);
	printf("\nThis is our array.\n");
	printf("Calculate the sum of the numbers between %d and %d element array.\n", i, j);
	sum = 0;
	for (z = i + 1; z < j; z++)
		sum = sum + arr[z];
	printf("The sum is %d\n", sum);
	
	return 0;
}
int random(int low, int upp)
{
	return low + rand() % (upp - low +1);
}